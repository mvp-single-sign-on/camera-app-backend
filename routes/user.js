const express = require('express');
const router = express.Router();
const request = require('request');
const config = require('./../config');
const { postFetchFormData, getFetch } = require('./../utils/fetch')
const queryString = require('query-string');



// router.get('/', async (req, res) => {

//   const dm = await request(
//     // POST request to /introspect endpoint
//     {
//       method: 'POST',
//       uri: `https://fusionauth.ilotusland.asia/oauth2/introspect`,
//       form: {
//         'client_id': config.clientID,
//         'token': req.session.token
//       }
//     }
//   )
//   console.log(dm.body, '====dm====')
//   // token in session -> get user data and send it back to the react app
//   if (req.session.token) {
//     request(
//       // POST request to /introspect endpoint
//       {
//         method: 'POST',
//         uri: `https://fusionauth.ilotusland.asia/oauth2/introspect`,
//         form: {
//           'client_id': config.clientID,
//           'token': req.session.token
//         }
//       },

//       // callback
//       (error, response, body) => {
//         let introspectResponse = JSON.parse(body);
//         console.log(introspectResponse, '==introspectResponse==')
//         // valid token -> get more user data and send it back to the react app
//         // expired token -> send nothing
//         if (!introspectResponse.active) {
//           req.session.destroy();
//           return res.send({});
//         }
//         // if (introspectResponse.active) {
//         request(
//           // GET request to /registration endpoint
//           {
//             method: 'GET',
//             uri: `https://fusionauth.ilotusland.asia/api/user/registration/${introspectResponse.sub}/${config.applicationID}`,
//             json: true,
//             headers: {
//               'Authorization': config.apiKey
//             }
//           },

//           // callback
//           (error, response, body) => {
//             if (error) {
//               console.log(error, '==error==')
//             }
//             console.log(body, '==body==response', respons)
//             // const userInfoResponse = JSON.parse(body)
//             // console.log(userInfoResponse, '==userInfoResponse==')
//             res.send(
//               {
//                 token: {
//                   ...introspectResponse,
//                 },
//                 ...body
//               }
//             );
//           }
//         );
//         // }


//       }
//     );
//   }

//   // no token -> send nothing
//   else {
//     res.send({});
//   }
// });

router.get('/', async (req, res) => {
  try {
    console.log(req.session.token, '==token======>get user ')
    if (!req.session.token) {
      return res.send({});
    }
    console.log('==start get user intro')
    const introspectResponse = await postFetchFormData(
      `https://fusionauth.ilotusland.asia/oauth2/introspect`,
      {
        'client_id': config.clientID,
        'token': req.session.token
      }
    )
    console.log(introspectResponse, '===get user intro done')
    // console.log(introResponse, '==introResponse==')



    if (!introspectResponse.active) {
      req.session.destroy();
      return res.send({});
    }

    const userInfo = await getFetch(`https://fusionauth.ilotusland.asia/api/user/${introspectResponse.sub}`)
    // console.log(userInfo, '==userInfo==')
    // console.log(`https://fusionauth.ilotusland.asia/api/user/${introspectResponse.sub}`)
    // const userRegistration = await getFetch(`https://fusionauth.ilotusland.asia/api/user/registration/${introspectResponse.sub}/${config.applicationID}`)
    // console.log(userRegistration, '==userRegistration==')
    // valid token -> get more user data and send it back to the react app
    // const userRegistration = await request(
    //   // GET request to /registration endpoint
    //   {
    //     method: 'GET',
    //     uri: `https://fusionauth.ilotusland.asia/api/user/registration/${introspectResponse.sub}/${config.applicationID}`,
    //     json: true,
    //     headers: {
    //       'Authorization': config.apiKey
    //     }
    //   }
    // )

    return res.send({ user: userInfo.user });


    // return res.send(
    //   {
    //     token: {
    //       ...introspectResponse,
    //     },
    //     ...body
    //   }
    // );




  } catch (error) {
    console.log(error.message)
    return res.send(error)
  }
})

module.exports = router;
