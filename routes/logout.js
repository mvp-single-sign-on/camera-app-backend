const express = require('express');
const router = express.Router();
const config = require('./../config');

router.get('/', (req, res) => {
  console.log('===log out user===')
  console.log(req.session.token, '==token======> before destroy ')

  // delete the session
  req.session.destroy();
  console.log(req.session, '==token======> after destroy ')


  // end FusionAuth session
  res.redirect(`https://fusionauth.ilotusland.asia/oauth2/logout?client_id=${config.clientID}`);
});

module.exports = router;
