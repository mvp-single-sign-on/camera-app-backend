const axios = require('axios')
const FormData = require('form-data');
const config = require('./../config')

const getHeaders = () => {
  var headers = {
    // Accept: 'application/json',
    Authorization: config.apiKey
  }

  return headers
}

function getFetch(url, params) {
  let attributes = Object.assign(
    {
      headers: getHeaders(),
      params,
    }
  )
  // console.log(url, '==url==')
  // console.log(attributes, '==attributes===')
  return new Promise((resolve, reject) => {
    axios
      .get(url, attributes)
      .then(res => {
        // if (res.status === 200) {
        resolve(res.data)
        // } else {
        //   reject({ error: true })
        // }
      })
      .catch(e => reject(e))
  })
}

async function postFetchFormData(url, data = {}) {
  var form = new FormData();
  Object.keys(data).map(key => {
    form.append(key, data[key])
  })
  // console.log(bodyFormData, '==bodyFormData==')
  const response = await axios.post(url, form, { headers: form.getHeaders() })
  return response.data

}

module.exports = { postFetchFormData, getFetch }